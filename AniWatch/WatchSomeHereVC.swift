//
//  WatchSomeHereVC.swift
//  AniWatch
//
//  Created by Nick Joliya on 03/09/23.
//

import UIKit

class WatchSomeHereVC: UIViewController {

    @IBOutlet weak var cvMainList: UICollectionView!
    var data : [MenuManager.AnimeItem] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        data = MenuManager.shared.watchOnYoutube
        cvMainList.delegate = self
        cvMainList.dataSource = self
    }
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension WatchSomeHereVC : UICollectionViewDataSource ,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CVCWatchHere", for: indexPath) as! CVCWatchHere
        cell.lblTitle.text = data[indexPath.row].title
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cvMainList.frame.size.width , height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // Replace "YOUR_VIDEO_ID" with the actual YouTube video ID or URL
              let videoID = data[indexPath.row].desc
              
              // Construct the URL to the YouTube video
              let youtubeURL = URL(string: "https://www.youtube.com/watch?v=\(videoID)")!
              
              // Check if the YouTube app is installed, and if so, open the video there
              if UIApplication.shared.canOpenURL(URL(string: "youtube://")!) {
                  UIApplication.shared.open(youtubeURL, options: [:], completionHandler: nil)
              } else {
                  // If the YouTube app is not installed, open the video in a web browser
                  UIApplication.shared.open(youtubeURL, options: [:], completionHandler: nil)
              }
    }
    
}

