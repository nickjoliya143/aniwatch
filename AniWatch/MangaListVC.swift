//
//  MangaListVC.swift
//  AniWatch
//
//  Created by Nick Joliya on 05/09/23.
//

import UIKit

class MangaListVC: UIViewController {

    @IBOutlet weak var cvMainList: UICollectionView!
    var data : [MenuManager.AnimeItem] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        data = MenuManager.shared.webForManga
        cvMainList.delegate = self
        cvMainList.dataSource = self
    }
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension MangaListVC : UICollectionViewDataSource ,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CVCReadSomeManga", for: indexPath) as! CVCReadSomeManga
        cell.lblTitle.text = data[indexPath.row].title
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cvMainList.frame.size.width , height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let url = URL(string: data[indexPath.row].desc) {
            // Open the URL in a web browser
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
}
