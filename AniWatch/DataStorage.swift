//
//  DataStorage.swift
//  MantraMelody
//
//  Created by Nick Joliya on 24/08/23.
//
import Foundation

import Foundation

class MenuManager {
    static let shared = MenuManager() // Singleton instance
    
    struct MenuItem {
        let title: String
        let description: String
        let imageURL: String // You can use URL type if the images are online
        
        init(title: String, description: String, imageURL: String) {
            self.title = title
            self.description = description
            self.imageURL = imageURL
        }
    }
    
    struct AnimeItem {
        let title: String
        let image: String
        let desc: String // You can use URL type if the images are online
        
        init(title: String, image: String, pdfName: String) {
            self.title = title
            self.image = image
            self.desc = pdfName
        }
    }
    
    let famousAnime : [AnimeItem]  = [
        AnimeItem(title: "DEMON SLAYER", image: "r1", pdfName: "Demon Slayer: Kimetsu no Yaiba (鬼滅の刃, Kimetsu no Yaiba, 'Blade of Demon Destruction'[4]) is a Japanese manga series written and illustrated by Koyoharu Gotouge. It was serialized in Shueisha's shōnen manga magazine Weekly Shōnen Jump from February 2016 to May 2020, with its chapters collected in 23 tankōbon volumes. It has been published in English by Viz Media and simultaneously published by Shueisha on their Manga Plus platform. It follows teenage Tanjiro Kamado, who strives to become a Demon Slayer after his family was slaughtered and his younger sister, Nezuko, is turned into a demon."),
        AnimeItem(title: "SKIP AND LOAFER", image: "r2", pdfName: "Skip and Loafer[a] (Japanese: スキップとローファー, Hepburn: Sukippu to Rōfā) is a Japanese manga series written and illustrated by Misaki Takamatsu. It has been serialized in Kodansha's Monthly Afternoon since August 2018. It has been licensed in North America for English release by Seven Seas Entertainment. An anime television series adaptation produced by P.A. Works aired from April to June 2023.By January 2023, the manga had over 1 million copies in circulation. In 2023, Skip and Loafer won the 47th Kodansha Manga Award in the general category."),
        AnimeItem(title: "HELL’S PARADISE", image: "r3", pdfName: "Hell's Paradise: Jigokuraku (Japanese: 地獄楽, Hepburn: Jigokuraku) is a Japanese manga series written and illustrated by Yuji Kaku. It was serialized weekly for free on Shueisha's Shōnen Jump+ application and website from January 2018 to January 2021, with its chapters collected in 13 tankōbon volumes. Set in the Edo period of Japan, it follows the ninja Gabimaru and the executioner Yamada Asaemon Sagiri as they search for the elixir of immortality. Viz Media has licensed the series for English release in North America. An anime television series adaptation produced by MAPPA aired from April to July 2023. A second season has been announced."),
        AnimeItem(title: "TENGOKU DAIMAKYO", image: "r4", pdfName: "Heavenly Delusion (Japanese: 天国大魔境, Hepburn: Tengoku Daimakyō, lit. 'Heaven Grand Makyō') is a Japanese manga series written and illustrated by Masakazu Ishiguro [jp]. It has been serialized in Kodansha's seinen manga magazine Monthly Afternoon since January 2018. An anime television series adaptation produced by Production I.G aired from April to June 2023."),
        AnimeItem(title: "SUMMER TIME RENDERING", image: "r5", pdfName: "Summer Time Rendering (Japanese: サマータイムレンダ, Hepburn: Samā Taimu Renda[a]) is a Japanese manga series written and illustrated by Yasuki Tanaka. It was serialized on Shueisha's digital magazine Shōnen Jump+ from October 2017 to February 2021. An anime television series adaptation produced by OLM aired from April to September 2022. A live-action film, a real escape game, and a video game have also been announced."),
        AnimeItem(title: "THE FIRE HUNTER", image: "r6", pdfName: "The Fire Hunter (火狩りの王, Hikari no Ō, The Firecatcher Lord) is a Japanese fantasy novel series written by Rieko Hinata and illustrated by Akihiro Yamada. Holp Shuppan have published four volumes between December 2018 and September 2020. An anime television series adaptation by Signal.MD aired from January to March 2023. A second season has been announced."),
        AnimeItem(title: "VINLAND SAGA SEASON 2", image: "r7", pdfName: "The second season of the Vinland Saga anime television series is produced by MAPPA and based on the manga series of the same name by Makoto Yukimura. Despite the studio change, the series retained its main production staff from the previous season. The series is directed by Shūhei Yabuta, with Hiroshi Seko handling series composition, Takahiko Abiru designing the characters[1] and Yutaka Yamada composing the music.[2] Taking place a year after the events of the first season, the season primarily focuses on a slave from England named Einar as he meets the protagonist, Thorfinn, while working in a farm."),
        AnimeItem(title: "TOKYO REVENGERS SEASON 2", image: "r8", pdfName: "Tokyo Revengers (Japanese: 東京卍リベンジャーズ[a], Hepburn: Tōkyō Ribenjāzu) is a Japanese manga series written and illustrated by Ken Wakui. It was serialized in Kodansha's Weekly Shōnen Magazine from March 2017 to November 2022, with its chapters collected in 31 tankōbon volumes. The story follows Takemichi Hanagaki, a 26-year-old freeter with a gloomy life, learns that his ex-girlfriend, Hinata Tachibana, has died in a dispute involving the Tokyo Manji Gang. The next day, while returning from a part-time job, Takemichi gets pushed off the subway platform by someone. As he is about to be hit, he jumps back in time, twelve years. He happens to have returned to the year he was dating Hinata and then, Takemichi makes the decision to do everything to prevent her death.")
    ]
    
    let trandingCharactor : [AnimeItem]  = [
        AnimeItem(title: "Goku", image: "g1", pdfName: "Son Goku[nb 20] is a character and the main protagonist of the Dragon Ball manga series created by Akira Toriyama. He is based on Sun Wukong (known as Son Goku in Japan and Monkey King in the West), a main character of the classic 16th century Chinese novel Journey to the West, combined with influences from the Hong Kong action cinema of Jackie Chan and Bruce Lee. Goku made his debut in the first Dragon Ball chapter, Bulma and Son Goku,[nb 21][nb 22] originally published in Japan's Weekly Shōnen Jump magazine on December 3, 1984.[2] Goku is introduced as an eccentric, monkey-tailed boy who practices martial arts and possesses superhuman strength. He meets Bulma and joins her on a journey to find the seven wish-granting Dragon Balls. Along the way, he finds new friends who follow him on his journey to become a stronger fighter. As Goku grows up, he becomes the Earth's mightiest warrior and battles a wide variety of villains with the help of his friends and family, while also gaining new allies in the process."),
        AnimeItem(title: "Naruto Uzumaki ", image: "g2", pdfName: "Naruto Uzumaki (Japanese: うずまき ナルト, Hepburn: Uzumaki Naruto) (/ˈnɑːrətoʊ/) is the titular protagonist of the manga Naruto, created by Masashi Kishimoto. As the series progresses, he is a young ninja from the fictional village of Konohagakure (Hidden Leaf Village). The villagers ridicule and ostracize Naruto on account of the Nine-Tailed Demon Fox—a malevolent creature that attacked Konohagakure—that was sealed away in Naruto's body. Despite this, he aspires to become his village's leader, the Hokage, in order to receive their approval. His carefree, optimistic, and boisterous personality enables him to befriend other Konohagakure ninja, as well as ninja from other villages. Naruto appears in the series' films and in other media related to the franchise, including video games and original video animations (OVA), as well as the sequel Boruto: Naruto Next Generations, where he is the Hokage, and his son, Boruto Uzumaki, is the protagonist."),
        AnimeItem(title: "Itachi Uchiha", image: "g3", pdfName: "Itachi Uchiha (うちは イタチ, Uchiha Itachi) is a character in the Naruto manga and anime series created by Masashi Kishimoto. Itachi is the older brother of Sasuke Uchiha, and is responsible for killing all the members of their clan, sparing only Sasuke. He appears working as a terrorist from the organisation Akatsuki and serves as Sasuke's greatest enemy. During the second part of the manga, Itachi becomes involved in attacks to ninjas possessing tailed-beast creatures until facing Sasuke in a one-on-one battle. Although Itachi perishes during the final duel, it is later revealed that Itachi had a secret reason for assassinating the Uchiha clan. Itachi is a playable character in most of the video games from the series."),
        AnimeItem(title: "Kakashi Hatake", image: "g4", pdfName: "Kakashi Hatake (Japanese: はたけ カカシ, Hepburn: Hatake Kakashi) is a fictional character and one of the main protagonists in the Naruto manga and anime series created by Masashi Kishimoto. In the story, Kakashi is the teacher of Team 7, consisting of the series' primary characters, Naruto Uzumaki, Sasuke Uchiha, and Sakura Haruno. Kakashi's past has been extensively explored in the series, resulting in a gaiden being devoted to his past experiences. Kakashi has appeared in several pieces of Naruto media, the featured films in the series, the original video animations, and the video games."),
        AnimeItem(title: "Saitama", image: "g5", pdfName: "Saitama (Japanese: サイタマ) is a fictional superhero and the protagonist of One's manga series One-Punch Man. Saitama, an unassociated hero, hails from Z-City and performs heroic deeds as a hobby. For three years, he has trained enough to defeat any enemy with a single punch, his unmatched strength leaving him bored. He becomes a reluctant mentor to Genos, a cyborg seeking revenge against another cyborg who killed his family and destroyed his hometown, after Saitama defeats a monster that defeated Genos. With Genos, Saitama learns about a heroes organization who are also fighting the monsters."),
        AnimeItem(title: "Roronoa Zoro", image: "g6", pdfName: "Roronoa Zoro (ロロノア・ゾロ, Roronoa Zoro, spelled as Roronoa Zolo in some English adaptations), also known as Pirate Hunter Zoro (海賊狩りのゾロ, Kaizoku-Gari no Zoro), is a fictional character created by Japanese manga artist Eiichiro Oda who appears in the manga series and media franchise One Piece. He first appeared in the third chapter of the One Piece manga, published in the manga magazine Weekly Shōnen Jump in 1997. Zoro is the first crewmate to join Monkey D. Luffy to be part of his crew of pirates, after he is rescued by Luffy from execution. Zoro is a highly skilled swordsman and serves as one of the crew's combatant, though he possesses an extremely poor sense of direction which recurs as a running gag throughout the series. In most voiced appearances, Zoro is voiced by actor Kazuya Nakai."),
        AnimeItem(title: "Monkey D. Luffy", image: "g7", pdfName: "Monkey D. Luffy (/ˈluːfi/ LOO-fee) (Japanese: モンキー・D・ルフィ, Hepburn: Monkī Dī Rufi, [ɾɯɸiː]), also known as Straw Hat Luffy[n 2], is a fictional character and the protagonist of the One Piece manga series, created by Eiichiro Oda. Luffy made his debut as a young boy who acquires the properties of rubber after accidentally eating one of the Devil Fruits that belonged to “Red Hair” Shanks.Monkey D. Luffy is the captain of the Straw Hat Pirates, and dreamt of being a pirate since childhood from the influence of his idol and mentor Red-Haired Shanks. At the age of 17, Luffy sets sail from the East Blue Sea to the Grand Line in search of the legendary treasure, One Piece, to succeed Gol D. Roger as King of the Pirates.")
    ]
    
    let webForAnime : [AnimeItem]  = [
        AnimeItem(title: "Crunchyroll", image: "r1", pdfName: "https://www.crunchyroll.com/"),
        AnimeItem(title: "Netflix", image: "r2", pdfName: "https://www.netflix.com/in/"),
        AnimeItem(title: "Anime-Planet", image: "r3", pdfName: "https://www.anime-planet.com/"),
        AnimeItem(title: "MyAnimeList", image: "r4", pdfName: "https://myanimelist.net/"),
        AnimeItem(title: "Hulu", image: "r5", pdfName: "https://www.hulu.com/hub/anime"),
        AnimeItem(title: "Amazon Prime Video", image: "r6", pdfName:"https://www.amazon.com/gp/video/storefront/ref=atv_cat_genre_anime?contentType=merch&contentId=animated"),
    ]
    
    let webForManga : [AnimeItem]  = [
        AnimeItem(title: "MangaDex", image: "r1", pdfName: "https://mangadex.org/"),
        AnimeItem(title: "MangaRock", image: "r2", pdfName: "https://mangarock.com/"),
        AnimeItem(title: "MangaReader", image: "r3", pdfName: "https://mangareader.to/"),
        AnimeItem(title: "MangaFox", image: "r4", pdfName: "https://mangafoxfull.com/manga/"),
        AnimeItem(title: "Manganelo", image: "r5", pdfName: "https://ww6.manganelo.tv/"),
        AnimeItem(title: "Crunchyroll Manga", image: "r6", pdfName:"https://www.crunchyroll.com/comics/manga"),
        AnimeItem(title: "VIZ Media", image: "r4", pdfName: "https://www.viz.com/"),
        AnimeItem(title: "BookWalker", image: "r5", pdfName: "https://global.bookwalker.jp/"),

    ]
    
    
    let watchOnYoutube : [AnimeItem]  = [
        AnimeItem(title: "The Aristocrat's Otherworldly Adventure", image: "r1", pdfName: "ZrKCfj3u2cs"),
        AnimeItem(title: "Hell and Paradise Episode 1-12 ", image: "r2", pdfName: "L1Sq2eH8VH8"),
        AnimeItem(title: "Psychic School Wars", image: "r3", pdfName: "y0y2ya6PMC8"),
        AnimeItem(title: "ANNE FRANK'S DIARY", image: "r4", pdfName: "qAIRFyR6NyQ"),
        AnimeItem(title: "Dororo", image: "r5", pdfName: "NF7xiEKZqO0"),
        AnimeItem(title: "REWIND In Hindi", image: "WMZppN2ZR9c", pdfName:"WMZppN2ZR9c"),

    ]
    
    
    
    
    let NewAnime : [AnimeItem]  = [
        AnimeItem(title: "Tokyo Revengers season 2", image: "n1", pdfName: "Tokyo Revengers (Japanese: 東京卍リベンジャーズ[a], Hepburn: Tōkyō Ribenjāzu) is a Japanese manga series written and illustrated by Ken Wakui. It was serialized in Kodansha's Weekly Shōnen Magazine from March 2017 to November 2022, with its chapters collected in 31 tankōbon volumes. The story follows Takemichi Hanagaki, a 26-year-old freeter with a gloomy life, learns that his ex-girlfriend, Hinata Tachibana, has died in a dispute involving the Tokyo Manji Gang. The next day, while returning from a part-time job, Takemichi gets pushed off the subway platform by someone. As he is about to be hit, he jumps back in time, twelve years. He happens to have returned to the year he was dating Hinata and then, Takemichi makes the decision to do everything to prevent her death."),
        AnimeItem(title: "Vinland Saga season 2", image: "n2", pdfName: "The second season of the Vinland Saga anime television series is produced by MAPPA and based on the manga series of the same name by Makoto Yukimura. Despite the studio change, the series retained its main production staff from the previous season. The series is directed by Shūhei Yabuta, with Hiroshi Seko handling series composition, Takahiko Abiru designing the characters[1] and Yutaka Yamada composing the music.[2] Taking place a year after the events of the first season, the season primarily focuses on a slave from England named Einar as he meets the protagonist, Thorfinn, while working in a farm."),
        AnimeItem(title: "Dr. Stone season 3", image: "n3", pdfName: "Dr. Stone is an anime television series produced by TMS Entertainment based on the manga of the same name written by Riichiro Inagaki, illustrated by Boichi, and published in Shueisha's Weekly Shōnen Jump magazine. 3,700 years after a mysterious light turns every human on the planet into stone, genius boy Senku Ishigami emerges from his petrification into a Stone World and seeks to rebuild human civilization from the ground up."),
        AnimeItem(title: "Demon Slayer season 3", image: "n4", pdfName: "Demon Slayer: Kimetsu no Yaiba is a Japanese anime television series based on the manga series of the same title, written and illustrated by Koyoharu Gotouge. At the end of the second-season finale, a third season covering the Swordsmith Village arc from the manga was announced.[1] The third season titled Demon Slayer: Kimetsu no Yaiba – Swordsmith Village Arc,[a] adapts volumes twelve to fifteen (chapters 98–127) of the manga series which aired from April 9 to June 18, 2023.[2][3][4] The season was directed by Haruo Sotozaki, with character designs by Akira Matsushima who also serves as a chief animation director, and animation produced by Ufotable; the main cast from the second season also returned.[1]"),
        AnimeItem(title: "Jujutsu Kaisen season 2", image: "n5", pdfName: "On February 12, 2022, a second season was announced.[2] Shōta Goshozono replaced Sunghoo Park as series director, with Sayaka Koiso and Tadashi Hiramatsu designing the characters and Yoshimasa Terui returning as the sole composer.[3] It premiered on July 6, 2023.[4] It will air for two continuous cours and adapt the manga's Hidden Inventory / Premature Death and Shibuya Incident story arcs.[5] Hidden Inventory acts as a prequel to Jujutsu Kaisen 0 and the first season. It focuses on Satoru Gojo and Suguru Geto during their time as students at Jujutsu High, and how they became enemies. Shibuya Incident takes place in the present as the Sorcerers and Curses engage in an all-out war in Shibuya on Halloween."),
        AnimeItem(title: "Attack on Titan The Last Season Part 3", image: "n6", pdfName: "The third season of the Attack on Titan anime television series was produced by IG Port's Wit Studio, chief directed by Tetsurō Araki and directed by Masashi Koizuka, with Yasuko Kobayashi handling series composition and Kyōji Asano providing character designs. It covers the Uprising (chapters 51–70) and Return to Shiganshina (chapters 71–90) arcs from the original manga by Hajime Isayama. The season's first 12 episodes were broadcast on NHK General TV from July 23 to October 15, 2018, before going into hiatus until April 29, 2019.[1] Adult Swim began airing Funimation's English dub on August 18, 2018.[2]"),
        //AnimeItem(title: "VINLAND SAGA SEASON 2", image: "n7", pdfName: "r7")
    ]
    let menuItems: [MenuItem] = [
        MenuItem(title: "Famous Anime", description: "Description for Item 4", imageURL: "https://example.com/item4.jpg"),
        MenuItem(title: "New Released Anime", description: "Description for Item 1", imageURL: "https://example.com/item1.jpg"),
        MenuItem(title: "Best Web To Watch Anime", description: "Description for Item 2", imageURL: "https://example.com/item2.jpg"),
        MenuItem(title: "Tranding Anime Characters", description: "Description for Item 3", imageURL: "https://example.com/item3.jpg"),
        MenuItem(title: "Watch On Youtube", description: "Description for Item 8", imageURL: "https://example.com/item8.jpg"),
        MenuItem(title: "Read Some Manga Here", description: "Description for Item 8", imageURL: "https://example.com/item8.jpg"),
        MenuItem(title: "About Us", description: "Description for Item 6", imageURL: "https://example.com/item6.jpg"),
        MenuItem(title: "Contact Us", description: "Description for Item 7", imageURL: "https://example.com/item7.jpg"),
        MenuItem(title: "Privacy Policy", description: "Description for Item 8", imageURL: "https://example.com/item8.jpg"),
      
       
    ]
   
    private init() { } // Private constructor to enforce singleton pattern
}
