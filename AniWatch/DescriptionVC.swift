//
//  DescriptionVC.swift
//  AniWatch
//
//  Created by Nick Joliya on 05/09/23.
//

import UIKit

class DescriptionVC: UIViewController {

    @IBOutlet weak var txtvDescription: UITextView!
    @IBOutlet weak var imgmain: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var strTitle = ""
    var strDesc = ""
    var strImage = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        txtvDescription.text = strDesc
        lblTitle.text = strTitle
        imgmain.image = UIImage(named: strImage)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
