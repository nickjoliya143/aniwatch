//
//  NewAnimeVC.swift
//  AniWatch
//
//  Created by Nick Joliya on 01/09/23.
//

import UIKit

class NewAnimeVC: UIViewController {

    @IBOutlet weak var cvMainList: UICollectionView!
    var data : [MenuManager.AnimeItem] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        data = MenuManager.shared.NewAnime
        cvMainList.delegate = self
        cvMainList.dataSource = self
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension NewAnimeVC : UICollectionViewDataSource ,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CVCFamousAnime", for: indexPath) as! CVCFamousAnime
        cell.lblTitle.text = data[indexPath.row].title
        cell.imgBg.image = UIImage(named: data[indexPath.row].image)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cvMainList.frame.size.width , height: 220)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DescriptionVC") as! DescriptionVC
        
        vc.strDesc = data[indexPath.row].desc
        vc.strTitle = data[indexPath.row].title
        vc.strImage = data[indexPath.row].image
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
