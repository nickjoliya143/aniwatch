//
//  CVCFamousAnime.swift
//  AniWatch
//
//  Created by Nick Joliya on 01/09/23.
//

import UIKit

class CVCFamousAnime: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgBg: UIImageView!
}
